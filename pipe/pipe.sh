#!/usr/bin/env bash

set -e

BUILDBASIS_API_KEY=${BUILDBASIS_API_KEY:?'BUILDBASE_API_KEY variable missing.'}
WEBHOOK_URL=${BUILDBASIS_WEBHOOK_URL:="https://api.buildbasis.com/api/v1/webhook"}

BUILDBASIS_STATUS="in_progress"
if [ ! -z "$BITBUCKET_EXIT_CODE" ]; then
  BUILDBASIS_STATUS="$BITBUCKET_EXIT_CODE"
fi

api_header="X-Api-Key: $BUILDBASIS_API_KEY"

response_code=$(curl -s -o buildbasis_response.txt -w "%{http_code}" \
                -X POST \
                -H 'Content-type: application/json' \
                -H 'X-Buildbasis-Platform: bitbucket' \
                -H "$api_header" \
                --data "{ \
                  \"tag\": \"$BITBUCKET_TAG\", \
                  \"job_number\": \"$BITBUCKET_BUILD_NUMBER\", \
                  \"build_url\": \"\", \
                  \"project_name\": \"$BITBUCKET_REPO_FULL_NAME\", \
                  \"commit_hash\": \"$BITBUCKET_COMMIT\", \
                  \"repository_url\": \"$BITBUCKET_GIT_HTTP_ORIGIN\", \
                  \"status\": \"$BUILDBASIS_STATUS\", \
                  \"branch\": \"$BITBUCKET_BRANCH\" \
                }" ${WEBHOOK_URL})
if [[ ${response_code} == 200 ]]; then
  echo "Sending data to BuildBasis succeeded"
  exit 0
else
  echo "Sending data to BuildBasis failed."
  cat buildbasis_response.txt
  exit 1
fi

response=$(cat ${curl_output_file})
info "HTTP Response: $(echo ${response})"

if [[ "${response}" = "ok" ]]; then
  success "Notification successful."
else
  fail "Notification failed."
fi
