FROM alpine:3.9

RUN apk add --update --no-cache bash curl

COPY pipe /
COPY pipe.yml README.md /

RUN chmod a+x /*.sh

ENTRYPOINT ["/pipe.sh"]
