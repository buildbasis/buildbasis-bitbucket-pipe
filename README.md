# BuildBasis Bitbucket Pipe

This pipe is used to send build data to the BuildBasis platform.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: buidlbasis/buildbasis-bitbucket-pipe:1.0.0
    variables:
      BUILDBASIS_API_KEY: "${BUILDBASIS_API_KEY}"
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| BUILDBASIS_API_KEY (*) | The api key for your organization |

_(*) = required variable._

## Prerequisites
You must set the `BUILDBASIS_API_KEY` environment variable in the repository settings.

## Examples

Example:

```yaml
script:
  - pipe: buidlbasis/buildbasis-bitbucket-pipe:1.0.0
    variables:
      BUILDBASIS_API_KEY: "${BUILDBASIS_API_KEY}"
```


## Support
If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by contact@buildbasis.com

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
